# API UniOne

API for https://unione.io/ (https://unione.ru/)

## Example send email

```php
$client = new SMusatov\UniOne\Client('xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx', Client::SERVER_EU);

$emailSend = (new SMusatov\UniOne\Request\EmailSend())
    ->addRecipient('user@example.com')
    ->setSubject('Test')
    ->setBodyPlainText('Hello from textplain')
    ->setFrom('noreply@domain-from.com', 'Domain From')
    ->setTrackRead(true)
    ->addAttachment('filename.txt', base64_encode(file_get_contents('filename.txt')));

$response = $client->sendRequest($emailSend);

print_r($response)
```

```php
Array
(
    [status] => success
    [job_id] => 1mXObN-0003EB-HQAp
    [emails] => Array
        (
            [0] => user@example.com
        )

    [http_code] => 200
)
```