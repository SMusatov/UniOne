<?php

namespace SMusatov\UniOne;


class Client
{
    const SERVER_EU = 'eu1';
    const SERVER_US = 'us1';

    protected \GuzzleHttp\Client $httpClient;

    public function __construct(string $token, string $server)
    {
        $config = [
            'base_uri' => "https://$server.unione.io/ru/transactional/api/v1/",
            'allow_redirects' => true,
            'headers' => [
                'Content-Type' => 'application/json',
                'Accept' => 'application/json',
                'Accept-Language' => 'ru-RU,ru',
                'Cache-Control' => 'no-cache',
                'Pragma' => 'no-cache',
                'X-API-KEY' => $token,
            ],
            'http_errors' => false,
            'synchronous' => true,
            'verify' => true,
            'version' => 1.0
        ];

        $this->httpClient = new  \GuzzleHttp\Client($config);
    }

    public function sendRequest(AbstractRequest $request): array
    {
        $body = empty($request->getData()) ? '{}' : json_encode($request->getData(), JSON_UNESCAPED_UNICODE);

        $response = $this->httpClient->post($this->classToMethod(get_class($request)), ['body' => $body]);

        $body = json_decode($response->getBody()->getContents(), true);
        $body['http_code'] = $response->getStatusCode();

        return $body;
    }

    protected function classToMethod(string $class): string
    {
        $class = mb_substr($class, mb_strrpos($class, '\\') + 1);
        $class = preg_replace('/([A-Z])/u', '/$1', $class);
        $class = trim($class, '/');
        $class = mb_strtolower($class);

        return $class . '.json';
    }

}