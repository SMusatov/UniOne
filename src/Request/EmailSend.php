<?php

namespace SMusatov\UniOne\Request;

use SMusatov\UniOne\AbstractRequest;


class EmailSend extends AbstractRequest
{
    protected array $data = [
        'message' => [
            'recipients' => [],
            'body' => [],
            'track_links' => 0,
            'track_read' => 1
        ]
    ];

    public function addRecipient(string $email, array $substitutions = [], array $metadata = []): self
    {
        $recipient = [
            'email' => $email,
            'substitutions' => $substitutions,
            'metadata' => $metadata
        ];

        $this->data['message']['recipients'][] = $recipient;

        return $this;
    }

    public function setTemplateId(string $templateId): self
    {
        $this->data['message']['template_id'] = $templateId;
        $this->setTemplateEngineVelocity(false);

        return $this;
    }

    public function setBodyHtml(string $html): self
    {
        $this->data['message']['body']['html'] = $html;

        return $this;
    }

    public function setBodyPlainText(string $text): self
    {
        $this->data['message']['body']['plaintext'] = $text;

        return $this;
    }

    public function setBodyAMP(string $amp): self
    {
        $this->data['message']['body']['amp'] = $amp;

        return $this;
    }

    public function setSkipUnsubscribe(bool $skipUnsubscribe = true): self
    {
        $this->data['message']['skip_unsubscribe'] = $skipUnsubscribe ? 1 : 0;

        return $this;
    }

    public function setTemplateEngineVelocity(bool $velocity = true): self
    {
        $this->data['message']['template_engine'] = $velocity ? 'velocity' : 'simple';

        return $this;
    }

    public function addGlobalSubstitution(string $key, string $value): self
    {
        $this->data['message']['global_substitutions'][$key] = $value;

        return $this;
    }

    public function addGlobalMetadata(string $key, string $value): self
    {
        $this->data['message']['global_metadata'][$key] = $value;

        return $this;
    }

    public function setSubject(string $subject): self
    {
        $this->data['message']['subject'] = $subject;

        return $this;
    }

    public function setFrom(string $email, string $name = ''): self
    {
        $this->data['message']['from_email'] = $email;

        if ($name !== '') {
            $this->data['message']['from_name'] = $name;
        }

        return $this;
    }

    public function setReplyTo(string $email): self
    {
        $this->data['message']['reply_to'] = $email;

        return $this;
    }

    public function setTrackLinks(bool $trackLinks = true): self
    {
        $this->data['message']['track_links'] = $trackLinks ? 1 : 0;

        return $this;
    }

    public function setTrackRead(bool $trackRead = true): self
    {
        $this->data['message']['track_read'] = $trackRead ? 1 : 0;

        return $this;
    }

    public function addHeader(string $header, string $value): self
    {
        $this->data['message']['headers'][$header] = $value;

        return $this;
    }

    public function addAttachment(string $name, string $content, string $mime = 'application/octet-stream'): self
    {
        $attach = [
            'type' => $mime,
            'name' => $name,
            'content' => $content
        ];

        $this->data['message']['attachments'][] = $attach;

        return $this;
    }

    public function addInlineAttachment(string $name, string $content, string $mime = 'application/octet-stream'): self
    {
        $attach = [
            'type' => $mime,
            'name' => $name,
            'content' => $content
        ];

        $this->data['message']['inline_attachments'][] = $attach;

        return $this;
    }

}