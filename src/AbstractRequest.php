<?php

namespace SMusatov\UniOne;


abstract class AbstractRequest
{
    protected array $data = [];

    public function getData(): array
    {
        return $this->data;
    }

}